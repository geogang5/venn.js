@Library('placer-shared-lib@1.34.9') _

generalPipeline(
  pipeline_type: "docker",
  makefile_path: "ci/Makefile",
  build_image: "gcr.io/loyal-world-659/build-tools-fe-20:v1.1.0",
  vault_secrets: [
      [
          path: 'kv/services/jfrog/devops-ci',
          engineVersion: 2,
          secretValues: [
            [envVar: 'JFROG_TOKEN', vaultKey: 'api_key'],
            [envVar: 'JFROG_USER', vaultKey: 'name'],
            [envVar: 'NPM_PASS', vaultKey: 'api_key'],
            [envVar: 'NPM_USER', vaultKey: 'name'],
            [envVar: 'NPM_EMAIL', vaultKey: 'email'],
          ]
      ]
  ],
  protected_branches: ["master"],
)
